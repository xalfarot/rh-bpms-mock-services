/**
 * 
 */
package com.solarvillage.bpms.mock.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.logging.Logger;

import javax.inject.Inject;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.Filters;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.solarvillage.bpms.mock.model.ElectricPermitRequest;
import com.solarvillage.bpms.mock.service.ElectricPermitService;
import com.solarvillage.bpms.mock.util.RequestStatus;


@RunWith(Arquillian.class)
public class ElectricPermitTest {
    @Deployment
    public static Archive<?> createTestArchive() {
        return ShrinkWrap.create(WebArchive.class, "test.war")
                .addPackages(true, 
                		Filters.exclude(ElectricPermitTest.class.getPackage()), 
                		"com.solarvillage.bpms.mock")
                .addAsResource("META-INF/test-persistence.xml", "META-INF/persistence.xml")
                .addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml")
                // Deploy our test datasource
                .addAsWebInfResource("test-ds.xml");
    }
    
    private static final String OWNER = new String("Xicote");

    @Inject
    ElectricPermitService service;

    @Inject
    Logger log;

    @Test
    public void testRegister() throws Exception {
    	log.info("Create electrical permit request");
    	ElectricPermitRequest request = new ElectricPermitRequest();
    	request.setOwner(OWNER);
    	request.setContactInformation("contactInformation");
    	request.setProyectInformation("proyectInformation");
    	request.setStatus(RequestStatus.PENDING.toString());
		service.registerRequest(request );
		assertNotNull(request.getId());
        log.info("Electrical permit request was persisted with id " + request.getId()
        			+ " - status '" + request.getStatus() + "'.");
    	
    	Long id = request.getId();;
        log.info("Updating electrical permit request status to ACCEPTED");
		service.updateStatus(id, RequestStatus.ACCEPTED.toString());
        request = null;
		request = service.find(id);
    	log.info("Electrical permit request status: " + request.getId()
			+ " - '" + request.getStatus() + "'.");
    	assertEquals(request.getStatus(), RequestStatus.ACCEPTED.toString());
    	
    	log.info("Updating electrical permit request status to REJECTED");
		service.updateStatus(id, RequestStatus.REJECTED.toString());
        request = null;
		request = service.find(id);
    	log.info("Electrical permit request status: " + request.getId()
			+ " - '" + request.getStatus() + "'.");
    	assertEquals(request.getStatus(), RequestStatus.REJECTED.toString());
    	
    }

}
