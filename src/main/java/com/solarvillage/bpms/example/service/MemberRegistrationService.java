/*
 * JBoss, Home of Professional Open Source
 * Copyright 2014, Red Hat, Inc. and/or its affiliates, and individual
 * contributors by the @authors tag. See the copyright.txt in the
 * distribution for a full listing of individual contributors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.solarvillage.bpms.example.service;
/*
import com.solarvillage.bpms.example.model.Member;

import javax.ejb.Stateless;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.persistence.NoResultException;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

import java.util.List;
import java.util.logging.Logger;
*/
public class MemberRegistrationService {
/*
    @Inject
    private Logger log;

    @Inject
    private MemberRepository repository;

    @Inject
    private Event<Member> memberEventSrc;
    
    public List<Member> listAllMembers() {
        return repository.findAllOrderedByName();
    }
    
    public Member lookupMemberById(long id) {
        Member member = repository.findById(id);
        if (member == null) {
            throw new WebApplicationException(Response.Status.NOT_FOUND);
        }
        return member;
    }
    
    public boolean emailAlreadyExists(String email) {
        Member member = null;
        try {
            member = repository.findByEmail(email);
        } catch (NoResultException e) {
        }
        return member != null;
    }
    
    public void register(Member member) throws Exception {
        log.info("Registering " + member.getName());
        repository.register(member);
        memberEventSrc.fire(member);
    }
    */
}
