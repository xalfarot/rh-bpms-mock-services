package com.solarvillage.bpms.mock.util;

public enum RequestStatus {
	PENDING, ACCEPTED, REJECTED
}
