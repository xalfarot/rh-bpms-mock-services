package com.solarvillage.bpms.mock.data;

import java.util.List;
import java.util.logging.Logger;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import com.solarvillage.bpms.mock.model.ElectricPermitRequest;

@ApplicationScoped
public class ElectricPermitRepository {
	
	@Inject
	private Logger log;
	
	@Inject
	private EntityManager em;
	
	public List<ElectricPermitRequest> getAllElectricalPermits(){
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<ElectricPermitRequest> criteria = cb.createQuery(ElectricPermitRequest.class);
		Root<ElectricPermitRequest> permit = criteria.from(ElectricPermitRequest.class);
		criteria.select(permit);
		TypedQuery<ElectricPermitRequest> q = em.createQuery(criteria);
		return q.getResultList();
	}
	
    public void register(ElectricPermitRequest permit) throws Exception {
        log.info("Registering " + permit.getOwner());
        em.persist(permit);
    }


    public ElectricPermitRequest find(Long id) {
    	return em.find(ElectricPermitRequest.class, id);
    }

    public ElectricPermitRequest findByOwner(String owner) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<ElectricPermitRequest> criteria = cb.createQuery(ElectricPermitRequest.class);
        Root<ElectricPermitRequest> request = criteria.from(ElectricPermitRequest.class);
        criteria.select(request).where(cb.equal(request.get("owner"), owner));
        return em.createQuery(criteria).getSingleResult();
    }
    
    public void updateRequest(ElectricPermitRequest permit) {
    	em.merge(permit);
    }

}
