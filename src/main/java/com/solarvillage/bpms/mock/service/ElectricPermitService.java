package com.solarvillage.bpms.mock.service;

import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

import com.solarvillage.bpms.mock.data.ElectricPermitRepository;
import com.solarvillage.bpms.mock.model.ElectricPermitRequest;
import com.solarvillage.bpms.mock.util.RequestStatus;

@Stateless
public class ElectricPermitService {
	
    @Inject
    private Logger log;
    
    @Inject
    ElectricPermitRepository repository;

    public void registerRequest(ElectricPermitRequest request) throws Exception {
        log.info("Registering request: " + request.toString());
        if(request.getOwner() == null || request.getContactInformation() == null
        		|| request.getProyectInformation() == null) {
        	log.severe("Owner, contact and project information must have a value.");
        	throw new WebApplicationException(Response.Status.BAD_REQUEST);
        }
        request.setStatus(RequestStatus.PENDING.toString());
        repository.register(request);
    }
    
    public ElectricPermitRequest find(Long id) {
    	ElectricPermitRequest request = repository.find(id);
        if (request == null) {
            throw new WebApplicationException(Response.Status.NOT_FOUND);
        }
        return request;    	
    }

    public ElectricPermitRequest findByOwner(String owner) {
    	ElectricPermitRequest request = repository.findByOwner(owner);
        if (request == null) {
            throw new WebApplicationException(Response.Status.NOT_FOUND);
        }
        return request;    	
    }
    
    public void updateStatus(Long id, String status) {
        if (RequestStatus.valueOf(status) == null) {
            throw new WebApplicationException(Response.Status.BAD_REQUEST);
        }
    	ElectricPermitRequest request = find(id);
        if (request == null) {
            throw new WebApplicationException(Response.Status.NOT_FOUND);
        }
        request.setStatus(status);
        repository.updateRequest(request);
        
    }

}
