package com.solarvillage.bpms.mock.model;

import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

/**
 * Entity implementation class for Entity: ResidentialElectricPermit
 *
 */
@SuppressWarnings("serial")
@Entity
@Table
public class ElectricPermitRequest implements Serializable {

    @Id
    @GeneratedValue
    private Long id;
	
	@NotNull
    @NotEmpty
    private String owner;
	
	@NotNull
    @NotEmpty
    private String contactInformation;

	@NotNull
    @NotEmpty
    private String proyectInformation;

	@NotNull
    @NotEmpty
    private String status;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public String getContactInformation() {
		return contactInformation;
	}

	public void setContactInformation(String contactInformation) {
		this.contactInformation = contactInformation;
	}

	public String getProyectInformation() {
		return proyectInformation;
	}

	public void setProyectInformation(String proyectInformation) {
		this.proyectInformation = proyectInformation;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String estatus) {
		this.status = estatus;
	}

	@Override
	public String toString() {
		return "ElectricPermitRequest [id=" + id + ", owner=" + owner + ", contactInformation=" + contactInformation
				+ ", proyectInformation=" + proyectInformation + ", estatus=" + status + "]";
	}
	
   
}
